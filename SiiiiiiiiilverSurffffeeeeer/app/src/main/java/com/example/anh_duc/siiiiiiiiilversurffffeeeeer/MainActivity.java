package com.example.anh_duc.siiiiiiiiilversurffffeeeeer;

import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ToggleButton;

public class MainActivity extends AppCompatActivity {

    private MediaPlayer silversurferButtonMP;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        Button acceptCallBtn = (Button) this.findViewById(R.id.acceptCallBtn);
        Button endCallBtn = (Button) this.findViewById(R.id.EndCallBtn);

        acceptCallBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopPlaying();
                silversurferButtonMP = MediaPlayer.create(MainActivity.this, R.raw.silversurfer);
                silversurferButtonMP.start();
            }
        });

        endCallBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopPlaying();
                silversurferButtonMP = MediaPlayer.create(MainActivity.this, R.raw.silversurfer);
            }
        });


    }

    private void stopPlaying(){
        if(silversurferButtonMP != null){
            silversurferButtonMP.stop();
            silversurferButtonMP.release();
            silversurferButtonMP = null;
        }
    }
}